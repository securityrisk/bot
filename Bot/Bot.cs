﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using S = DocumentFormat.OpenXml.Spreadsheet.Sheets;
using E = DocumentFormat.OpenXml.OpenXmlElement;
using A = DocumentFormat.OpenXml.OpenXmlAttribute;

namespace Bot
{
    class Bot
    {
        //The method running by the thread to constantly check for new messages
        public static void checkReader()
        {
            //Catch any exceptions but this would most likely be a problem with the channel properties file
            try
            {
                while (irc.Connected)
                {
                    //read the line from irc
                    string rawMessage = reader.ReadLine();

                    //split the message into 4 parts
                    String[] message = rawMessage.Split(' ');

                    //show the message on the console
                    Console.WriteLine(rawMessage);

                    //if the message is a ping from the twitch server then send back pong immediately
                    //if it's not a ping message then it's a private message
                    //the bot only cares about messages that start with an !
                    //so do all necessary operations on the message
                    if (message[0].Equals("PING"))
                    {
                        writer.WriteLine("PONG " + message[1]);
                        Console.WriteLine("PONG " + message[1]);
                    }
                    else if (message[1].Equals("PRIVMSG"))
                    {
                        //split the actual message part from the received message
                        string msg = rawMessage.Substring(rawMessage.IndexOf(":", 1) + 1);

                        //check if the message starts with an !
                        if (msg.StartsWith("!"))
                        {
                            //check if the message is the command !<currency> or !xp
                            if (msg.Substring(msg.IndexOf("!") + 1).Equals(currency) || msg.Substring(msg.IndexOf("!") + 1).Equals("xp"))
                            {
                                //split the user from the received message
                                string user = message[0].Substring(1, message[0].IndexOf("!") - 1);

                                //check if there was no user meaning it was not split correctly
                                if (user != null)
                                {
                                    string cur;
                                    //check the user's currency and if the user is not there then add the user
                                    //As well print back through the bot the message of the currency
                                    if ((cur = getValue(user, CURRENCY)) != "")
                                        write(currency + ": " + user + "(" + getValue(user, CURRENCY) + ")");
                                    else
                                    {
                                        addUser(user);
                                        write("You have not yet accumulated currency");
                                    }
                                }
                                else
                                {
                                    //the user was not split correctly so send an error message
                                    Console.WriteLine("The message did not contain a user");
                                }
                            }
                        }
                    }
                    //arbitrary sleep call to the thread
                    Thread.Sleep(0);
                }
            }
            catch (Exception e)
            {
                //There's an exception and we print the exception and the stack trace as well as form our own message of speculation
                Console.WriteLine(e);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine("It seems there's a problem in the channel properties file.\nPlease revisit that file and make sure it is correct");
            }
        }
        /*
        private static void addCurrency(string p, int amount)
        {
            dbr = new StreamReader(winDir + "/User Database.txt");
            //FileStream q = new FileStream(winDir + "/User Database.txt", FileMode.Create);
            
            string temp = null;
            object[] targets = new object[2];
            targets[0] = "";
            targets[1] = new Int32;
            sc.Scan("Hello there. -1 2 3.0", "Hello {0} {1} {2} {3}", targets);
            /*while ((temp = dbr.ReadLine()) != " ")
            {
                if (temp.Substring(0, temp.IndexOf(" ")).Equals(p))
                {
                    int num = Convert.ToInt32(temp.Substring(temp.IndexOf(" ") + 1)) + 1;
                    dbr.Close();
                    q.Seek(0, SeekOrigin.End);
                }
            }
            dbr.Close();
        }
        
        private static void addUser(string p)
        {
            dbw = File.AppendText(winDir + "/User Database.txt");
            dbw.WriteLine(p + " " + 0);
            dbw.Close();
            write(p + ", You haven't acquired any " + currency +" yet. Stay a while.");
        }
        
        private static int getCurrency(string p)
        {
            dbr = new StreamReader(winDir + "/User Database.txt");
            string temp = null;
            while ((temp = dbr.ReadLine()) != " ")
            {
                if (temp.Substring(0, temp.IndexOf(" ")).Equals(p))
                {
                    return Convert.ToInt32(temp.Substring(temp.IndexOf(" ") + 1));
                }
            }
            dbr.Close();
            return 0;
        }

        private static bool checkDatabase(string p)
        {
            dbr = new StreamReader(winDir + "/User Database.txt");
            String temp = null;
            while ((temp = dbr.ReadLine()) != "" && temp != null && temp != "")
            {
                if (temp.Substring(0, temp.IndexOf(" ")).Equals(p))
                {
                    dbr.Close();
                    return true;
                }
            }
            dbr.Close();
            return false;
        }*/

        public static TcpClient irc;                                            //the TCP Client for IRC
        public static StreamReader reader;                                      //the reader for the irc stream
        public static StreamWriter writer;                                      //the writer for the irc stream
        public static String password = null;                                   //password for the bot
        public static String nick = null;                                       //the bot's name
        public static String channel = null;                                    //the channel for the bot to enter
        public static String currency = null;                                   //the currency to use
        public static StreamReader propertiesReader;                            //the reader of the Channel Properties text file
        public static string winDir = System.Environment.CurrentDirectory;      //the current directory
        public static readonly string USERNAME = "A", CURRENCY = "B",           //Each Column in the Spreadsheet
            SUBSCRIBER = "C", BATTLETAG = "D", TICKETS = "E", EXP = "F";

        static void Main(string[] args)
        {
            //Ignore this, it will be moved later
            string address = "A2";
            string value;
            int count = 2;
            while ((value = GetCellValue(winDir + "/Database.xlsx", "Sheet1", address)) != "")
            {
                Console.WriteLine(value);
                if (count != 2)
                    address = (count % 2) == 0 ? "A" + count : "B" + (count - 1);
                else
                    address = "B2";
                count++;
            }
            //End ignore
            //Declare properties reader and read the Channel Properties.txt file
            propertiesReader = new StreamReader(winDir + "/Channel Properties.txt");
            channel = propertiesReader.ReadLine().Substring(8);
            nick = propertiesReader.ReadLine().Substring(5);
            password = propertiesReader.ReadLine().Substring(9);
            currency = propertiesReader.ReadLine().Substring(9);

            //Write out everything gotten from the Channel Properties.txt file for clarity
            Console.WriteLine("Channel: " + channel + "\nNickname: " + nick + "\nCurrency: " + currency);
            Console.WriteLine("Connecting to " + channel);

            //Connect to the Server
            connectToServer();

            //Create the main thread to be run for checking for messages
            Thread t = new Thread(new ThreadStart(checkReader));
            t.Start();

            //Finally give the message of connection
            Console.WriteLine("Connected to " + channel);
        }

        private static void connectToServer()
        {
            //Create the new TCP Client
            irc = new TcpClient();

            //Try connecting with the given IP and port, if it doesn't work give an error message
            try
            {
                irc.Connect("199.9.250.229", 443);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot connect to the desired IP. This probably means you don't have internet access, or access to twitch.tv");
                
                //Exiting with code error
                System.Environment.Exit(1);
            }

            //Another check to see if we connected
            if (!irc.Connected)
                Console.WriteLine("Unable to connect to server");

            //Declare the reader and writer
            reader = new StreamReader(irc.GetStream());
            writer = new StreamWriter(irc.GetStream());

            //Have to flush the writer before writing anything, either by using AutoFlush or flushing after every write
            writer.AutoFlush = true;

            //write directly to the connected TCP Client and follow IRC API for joining an IRC.
            writer.WriteLine("PASS " + password);
            writer.WriteLine("NICK " + nick);
            writer.WriteLine("USER " + nick + " 8 * :" + nick);
            writer.WriteLine("JOIN " + channel);
        }

        //A method to take care of writing the bot messages. Just insert a string and the message will be sent to IRC
        public static void write(string s)
        {
            //Standard message format
            writer.WriteLine("PRIVMSG " + channel + " :" + s);
        }

        // Given a document name and text, 
        // inserts a new work sheet and writes the text to cell "A1" of the new worksheet.

        public static void InsertText(string docName, string text, string address)
        {
            // Open the document for editing.
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
            {
                WorkbookPart wbPart = spreadSheet.WorkbookPart;

                // Get the SharedStringTablePart. If it does not exist, create a new one.
                SharedStringTablePart shareStringPart;
                if (spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().Count() > 0)
                {
                    shareStringPart = spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
                }
                else
                {
                    shareStringPart = spreadSheet.WorkbookPart.AddNewPart<SharedStringTablePart>();
                }

                // Find the sheet with the supplied name, and then use that 
                // Sheet object to retrieve a reference to the first worksheet.
                Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Sheet1").FirstOrDefault();

                // Insert the text into the SharedStringTablePart.
                int index = InsertSharedStringItem(text, shareStringPart);

                // Retrieve worksheet
                WorksheetPart worksheetPart = //InsertWorksheet(spreadSheet.WorkbookPart);
                (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                // Insert cell A1 into the new worksheet.
                Cell cell = InsertCellInWorksheet(System.Text.RegularExpressions.Regex.Split(address, @"\d+")[0], uint.Parse(System.Text.RegularExpressions.Regex.Split(address, @"\D+")[1]), worksheetPart);

                // Set the value of cell A1.
                cell.CellValue = new CellValue(index.ToString());
                cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);

                // Save the new worksheet.
                worksheetPart.Worksheet.Save();
            }
        }

        // Given text and a SharedStringTablePart, creates a SharedStringItem with the specified text 
        // and inserts it into the SharedStringTablePart. If the item already exists, returns its index.
        private static int InsertSharedStringItem(string text, SharedStringTablePart shareStringPart)
        {
            // If the part does not contain a SharedStringTable, create one.
            if (shareStringPart.SharedStringTable == null)
            {
                shareStringPart.SharedStringTable = new SharedStringTable();
            }

            int i = 0;

            // Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
            foreach (SharedStringItem item in shareStringPart.SharedStringTable.Elements<SharedStringItem>())
            {
                if (item.InnerText == text)
                {
                    return i;
                }

                i++;
            }

            // The text does not exist in the part. Create the SharedStringItem and return its index.
            shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(text)));
            shareStringPart.SharedStringTable.Save();

            return i;
        }

        // Given a WorkbookPart, inserts a new worksheet.
        private static WorksheetPart InsertWorksheet(WorkbookPart workbookPart)
        {
            // Add a new worksheet part to the workbook.
            WorksheetPart newWorksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());
            newWorksheetPart.Worksheet.Save();

            Sheets sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
            string relationshipId = workbookPart.GetIdOfPart(newWorksheetPart);

            // Get a unique ID for the new sheet.
            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Count() > 0)
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            string sheetName = "Sheet" + sheetId;

            // Append the new worksheet and associate it with the workbook.
            Sheet sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = sheetName };
            sheets.Append(sheet);
            workbookPart.Workbook.Save();

            return newWorksheetPart;
        }

        // Given a column name, a row index, and a WorksheetPart, inserts a cell into the worksheet. 
        // If the cell already exists, returns it. 
        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }

        // Retrieve the value of a cell, given a file name, sheet name, 
        // and address name.
        public static string GetCellValue(string fileName, string sheetName, string addressName)
        {
            string value = null;

            // Open the spreadsheet document for read-only access.
            using (SpreadsheetDocument document = SpreadsheetDocument.Open(fileName, false))
            {
                // Retrieve a reference to the workbook part.
                WorkbookPart wbPart = document.WorkbookPart;

                // Find the sheet with the supplied name, and then use that 
                // Sheet object to retrieve a reference to the first worksheet.
                Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetName).FirstOrDefault();

                // Throw an exception if there is no sheet.
                if (theSheet == null)
                {
                    throw new ArgumentException("sheetName");
                }

                // Retrieve a reference to the worksheet part.
                WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                // Use its Worksheet property to get a reference to the cell 
                // whose address matches the address you supplied.
                Cell theCell = wsPart.Worksheet.Descendants<Cell>().Where(c => c.CellReference == addressName).FirstOrDefault();

                // If the cell does not exist, return an empty string.
                if (theCell != null)
                {
                    value = theCell.InnerText;

                    // If the cell represents an integer number, you are done. 
                    // For dates, this code returns the serialized value that 
                    // represents the date. The code handles strings and 
                    // Booleans individually. For shared strings, the code 
                    // looks up the corresponding value in the shared string 
                    // table. For Booleans, the code converts the value into 
                    // the words TRUE or FALSE.
                    if (theCell.DataType != null)
                    {
                        switch (theCell.DataType.Value)
                        {
                            case CellValues.SharedString:

                                // For shared strings, look up the value in the
                                // shared strings table.
                                var stringTable = wbPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                                // If the shared string table is missing, something 
                                // is wrong. Return the index that is in
                                // the cell. Otherwise, look up the correct text in 
                                // the table.
                                if (stringTable != null)
                                {
                                    value = stringTable.SharedStringTable.ElementAt(int.Parse(value)).InnerText;
                                }
                                break;

                            case CellValues.Boolean:
                                switch (value)
                                {
                                    case "0":
                                        value = "FALSE";
                                        break;
                                    default:
                                        value = "TRUE";
                                        break;
                                }
                                break;
                        }
                    }
                }
            }
            return value;
        }

        //Finds the Address of the User in the Database
        public static string getAddress(string user)
        {

            string address = "A1";      //Starting with the first Cell A1
            string value;               //Stores the value of the Cell
            int count = 1;              //Start with a count of 1 signifying what row

            //while getting the cell value at the current address is not null then check to see if
            //it is equal to the user and return the address
            while ((value = GetCellValue(winDir + "/Database.xlsx", "Sheet1", address)) != "")
            {
                //Checks equality with user and if true return address
                if (value.Equals(user))
                    return address;

                //Iterate through the rows
                address = "A" + count;
                count++;
            }

            //if the user doesn't exist in the database then return an empty string
            return "";
        }

        //Gets the currency of the given user
        public static string getValue(string user, string column)
        {
            string address = "A2";      //Starting with the first Cell A1
            string value;               //Stores the value of the Cell
            int count = 2;              //Start with a count of 1 signifying what row

            //while getting the cell value at the current address is not null then check to see if
            //it is equal to the user and return the currency of that user
            while ((value = GetCellValue(winDir + "/Database.xlsx", "Sheet1", address)) != "")
            {
                //Checks equality with user and if true return the currency
                if (value.Equals(user))
                    return GetCellValue(winDir + "/Database.xlsx", "Sheet1", column + count);

                //Iterate through the rows
                address = "A" + count;
                count++;
            }
            //if the user doesn't exist in the database then return an empty string
            return "";
        }

        public static int getLastIndex()
        {
            string address = "A2";      //Starting with the first Cell A1
            string value;               //Stores the value of the Cell
            int count = 2;              //Start with a count of 1 signifying what row

            //while getting the cell value at the current address is not null then check to see if
            //it is equal to the user and return the currency of that user
            while ((value = GetCellValue(winDir + "/Database.xlsx", "Sheet1", address)) != "")
            {
                //Iterate through the rows
                address = "A" + count;
                count++;
            }
            //if the user doesn't exist in the database then return an empty string
            return count;
        }

        //Add the user to the Database
        public static void addUser(string user)
        {
            int index = getLastIndex();
            InsertText(winDir + "/Database.xlsx", user, USERNAME + index);
            InsertText(winDir + "/Database.xlsx", "0", CURRENCY + index);

        }

        
    }
}